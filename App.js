import React, { Component } from 'react';
import { Root } from 'native-base';
import AWSAppSyncClient from 'aws-appsync';
import { Rehydrated } from 'aws-appsync-react';
import { ApolloProvider } from 'react-apollo';
import AppSync from './AppSync';
import Amplify, { Auth } from 'aws-amplify';
import { createRootNavigator } from './src/routers';
import awsConfig from './aws-exports';

Amplify.configure(awsConfig);

async function setClient() {
  try {
    const currentSession = await Auth.currentSession();

    return currentSession.getAccessToken().getJwtToken();
  } catch(err) {
    return '';
  }
}

const client = new AWSAppSyncClient({
  url: AppSync.graphqlEndpoint,
  region: AppSync.region,
  auth: {
    type: AppSync.authenticationType,
    jwtToken: setClient()
  }
});

export default class App extends Component {
  render() {
    const Layout = createRootNavigator;

    return (
      <ApolloProvider client={client}>
        <Rehydrated>
          <Root>
            <Layout/>
          </Root>
        </Rehydrated>
      </ApolloProvider>
    );
  }
}

