import React from 'react';
import {
  createSwitchNavigator
} from 'react-navigation';
import { SignedIn } from './SignedIn';
import { SignedOut } from './SignedOut';
import { SetupFlowStack } from './SetupFlow';
import { SplashScreen } from '../screens/SplashScreen';

export const createRootNavigator = createSwitchNavigator(
  {
    SplashScreen: {
      screen: SplashScreen
    },
    SignedIn: {
      screen: SignedIn
    },
    SignedOut: {
      screen: SignedOut
    },
    SetupFlowStack: {
      screen: SetupFlowStack
    }
  },
  {
    initialRouteName: 'SplashScreen'
  }
);
