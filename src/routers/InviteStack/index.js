import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { InviteMemberScreen } from '../../screens/InviteMemberScreen';
import { AppHeader } from '../../components/AppHeader';
import { HEADER_TYPES } from '../../constants';

export const InviteStack = createStackNavigator({
  Invite: {
    screen: InviteMemberScreen,
    navigationOptions: {
      title: 'Invite Member',
      header: props => <AppHeader type={HEADER_TYPES.PRIMARY} {...props} />
    }
  }
});
