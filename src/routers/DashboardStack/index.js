import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { DashboardScreen } from '../../screens/DashboardScreen';
import { AppHeader } from '../../components/AppHeader';
import { HEADER_TYPES } from '../../constants';

export const DashboardStack = createStackNavigator({
  Dashboard: {
    screen: DashboardScreen,
    navigationOptions: {
      title: 'Dashboard',
      header: props => <AppHeader type={HEADER_TYPES.PRIMARY} {...props} />
    }
  }
});
