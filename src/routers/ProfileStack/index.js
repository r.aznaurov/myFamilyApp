import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { ProfileScreen } from '../../screens/ProfileScreen';
import { AppHeader } from '../../components/AppHeader';
import { HEADER_TYPES } from '../../constants';

export const ProfileStack = createStackNavigator({
  Profile: {
    screen: ProfileScreen,
    navigationOptions: {
      title: 'Profile',
      header: props => <AppHeader type={HEADER_TYPES.PRIMARY} {...props} />
    }
  }
});
