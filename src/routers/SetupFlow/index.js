import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { SetProfileScreen } from '../../screens/SetProfileScreen';
import { JoinCreateScreen } from '../../screens/JoinCreateScreen';
import { CreateAccountScreen } from '../../screens/CreateAccount';
import { SelectModulesScreen } from '../../screens/SelectModules';

export const SetupFlowStack = createStackNavigator({
  SetProfile: {
    screen: SetProfileScreen,
    navigationOptions: {
      header: null
    }
  },
  JoinCreate: {
    screen: JoinCreateScreen,
    navigationOptions: {
      header: null
    }
  },
  CreateAccount: {
    screen: CreateAccountScreen,
    navigationOptions: {
      header: null
    }
  },
  SelectModules: {
    screen: SelectModulesScreen,
    navigationOptions: {
      header: null
    }
  }
});
