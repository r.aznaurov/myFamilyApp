import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import { ProfileStack } from '../ProfileStack';
import { SettingsStack } from '../SettingsStack';
import { InviteStack } from '../InviteStack';
import { DashboardStack } from '../DashboardStack';
import { SideBar } from '../../modules/SideBar';
import { ShoppingCartStack } from '../ShoppingCartStack';

export const SignedIn = createDrawerNavigator(
  {
    Dashboard: {
      screen: DashboardStack
    },
    Settings: {
      screen: SettingsStack
    },
    Profile: {
      screen: ProfileStack
    },
    InviteMember: {
      screen: InviteStack
    },
    ShoppingCart: {
      screen: ShoppingCartStack
    }
  },
  {
    contentComponent: props => <SideBar {...props} />
  }
);

