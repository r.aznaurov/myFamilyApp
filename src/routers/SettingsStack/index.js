import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { SettingsScreen } from '../../screens/SettingsScreen';
import { AppHeader } from '../../components/AppHeader';
import { HEADER_TYPES } from '../../constants';

export const SettingsStack = createStackNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      title: 'Settings',
      header: props => <AppHeader type={HEADER_TYPES.PRIMARY} {...props} />
    }
  }
});
