import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { ShoppingCartListScreen } from '../../screens/ShoppingCartListScreen';
import { ShoppingCartScreen } from '../../screens/ShoppingCartScreen';
import { AppHeader } from '../../components/AppHeader';
import { HEADER_TYPES } from '../../constants';

export const ShoppingCartStack = createStackNavigator({
  ShoppingCartList: {
    screen: ShoppingCartListScreen,
    navigationOptions: {
      title: 'Shopping Cart',
      header: props => <AppHeader type={HEADER_TYPES.PRIMARY} {...props} />
    }
  },
  ShoppingCart: {
    screen: ShoppingCartScreen,
    navigationOptions: {
      title: 'ShoppingCartItem',
      //TODO: Сюда пробрасывать название корзины или дату или как решим ее назвать вообще?
      header: props => <AppHeader {...props} />
    }
  }
});
