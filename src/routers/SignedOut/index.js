import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { SignInScreen } from '../../screens/SignInScreen';
import { SignUpScreen } from '../../screens/SignUpScreen';
import { SignUpConfirmScreen } from '../../screens/SignUpConfirmScreen';

export const SignedOut = createStackNavigator({
  SignIn: {
    screen: SignInScreen,
    navigationOptions: {
      title: 'Sign in',
      header: null
    }
  },
  SignUp: {
    screen: SignUpScreen,
    navigationOptions: {
      title: 'Sign up',
      header: null
    }
  },
  SignUpConfirm: {
    screen: SignUpConfirmScreen,
    navigationOptions: {
      title: 'Confirm Registration',
      header: null
    }
  }
});
