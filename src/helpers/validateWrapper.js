import { validation } from './validation';
import validatejs from 'validate.js';

export const validate = (fieldName, value, value2) => {
  const formValues = {};

  if (fieldName === 'confirmPassword') {
    formValues['password'] = value;
    formValues['confirmPassword'] = value2;
  } else {
    formValues[fieldName] = value;
  }

  const validationRulesObj = {};
  validationRulesObj[fieldName] = validation[fieldName];

  const result = validatejs(formValues, validationRulesObj);

  if (result) {
    return result[fieldName][0];
  }

  return null;
};

