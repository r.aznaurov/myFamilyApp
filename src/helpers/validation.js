export const validation = {
  email: {
    presence: {
      message: '^Please enter an email address'
    },
    email: {
      message: '^Please enter a valid email address'
    }
  },
  name: {
    presence: true,
    length: {
      minimum: 1,
      maximum: 20
    },
    format: {
      pattern: '[a-z0-9]+',
      flags: 'i',
      message: '^Can only contain a-z and 0-9'
    }
  },
  password: {
    presence: {
      message: '^Please enter a password'
    },
    format: {
      pattern: '^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$',
      flags: 'g',
      message: 'Must contain  at last one lowercase, one uppercase and one integer'
    },
    length: {
      minimum: 6,
      message: '^Your password must be at least 6 characters'
    }
  },
  confirmPassword: {
    presence: {
      message: '^Please repeat password to confirm'
    },
    equality: {
      attribute: 'password',
      message: '^The passwords don\'t not match'
    }
  },
  confirmCode: {
    presence: {
      message: '^Please enter confirmation code from your email'
    },
    numericality: {
      onlyInteger: true,
      message: '^Confirm code should contain only integers'
    },
    length: {
      is: 6,
      message: '^Confirm code must has 6 integers'
    }
  },
  inviteCode: {
    presence: {
      message: '^Please enter invite code'
    },
    length: {
      minimum: 6,
      message: '^Minimum number of symbols is 6'
    },
    format: {
      pattern: '[a-z0-9]+',
      flags: 'i',
      message: '^Can only contain a-z and 0-9'
    }
  }
};
