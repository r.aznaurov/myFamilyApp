import { AsyncStorage } from 'react-native';

export const storeDataToAsyncStorage = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (error) {
    console.log('AsyncStorage: ', error.message);
  }
};

export const retrieveItemFromAsyncStorage = async key => {
  try {
    const retrievedItem =  await AsyncStorage.getItem(key);
    const item = JSON.parse(retrievedItem);

    return item;
  } catch (error) {
    console.log(error.message);
  }
  return;
};
