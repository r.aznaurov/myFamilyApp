import languages from './languages';

const KEY = Symbol.for('app.languages.container');
const global_symbols = Object.getOwnPropertySymbols(global);

if(!global_symbols.includes(KEY)) {
  global[KEY] = {};
}

export const LANGS = {
  Set: (key, lang, value) => {
    return global[KEY][lang + key] = value;
  },
  Get: (key, lang) => {
    return global[KEY][lang + key] || `ERROR ${key}`;
  },
  Setup: languages => {
    Object.keys(languages).forEach(key => {
      Object.keys(languages[key]).forEach(lang => {
        this.Set(key, lang, languages[key][lang]);
      });
    });
  }
};

LANGS.Setup(languages);
