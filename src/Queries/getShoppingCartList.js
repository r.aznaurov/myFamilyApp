import gql from 'graphql-tag';

export const GET_SHOPPING_CART_LIST = gql`
  query GetShoppingCartList {
    ListCarts {
      result {
        id
        name
        created_at
        created_by
      }
    }
  }
`;
