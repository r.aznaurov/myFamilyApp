import gql from 'graphql-tag';

export const GET_FAMILY_MEMBERS = gql`
  query GetFamilyMembers {
    ListFamilyMembers {
      result {
        id
        family_id
        first_name
        last_name
        role
      }
    }
  }
`;
