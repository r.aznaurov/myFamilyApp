import gql from 'graphql-tag';

export const GET_PRODUCTS_LIST = gql`  
  query getProductList ($cart_id: String){
    ListCartProducts (
      cart_id: $cart_id
    ) {
      result {
        id
        cart_id
        name
        quantity
        units
        type
        created_by
        created_at
        updated_by
        updated_at
        bought_by
        bought_at
      }
    }
  }
`;
