import gql from 'graphql-tag';

export const GET_ME = gql`
  query ME {
    Me {
      id
      family_id
      first_name
      last_name
      role
      family {
        id
        name
        modules
        created_at
      }
    }
  }
`;
