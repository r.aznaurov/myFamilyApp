import gql from 'graphql-tag';

export const ACTIVATE_INVITE_CODE = gql`
  mutation ActivateInviteCode(
    $invite_code: String!
  ) { 
    ActivateInviteCode( 
      input: { 
        invite_code: $invite_code
      } 
    ) { 
      id
      family_id
      first_name 
      last_name 
      role 
    } 
  }
`;


