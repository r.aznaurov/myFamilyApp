import gql from 'graphql-tag';

export const CREATE_FAMILY = gql`
  mutation CreateFamily (
    $name: String!,
    $modules: [FamilyModulesEnum]!
  ){
    CreateFamily (
      input: {
        name: $name
        modules: $modules
      }
    ) {
      id
      family_id
      first_name
      last_name
      role
      family {
        id
        name
        modules
        created_at
      }
    }
  }
`;
