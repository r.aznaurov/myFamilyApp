import gql from 'graphql-tag';

export const CREATE_INVITE_CODE = gql`
  mutation CreateInviteCode {
    CreateInviteCode {
      result {
        invite_code expire
      }
    }
  }
`;
