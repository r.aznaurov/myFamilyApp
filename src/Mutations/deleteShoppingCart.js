import gql from 'graphql-tag';

export const DELETE_SHOPPING_CART = gql`
  mutation DeleteCart ($id: String) {
    DeleteCart (
      input: {
        id: $id
      }
    ) {
      result {
        id
      }
    }
  }
`;
