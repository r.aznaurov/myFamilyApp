import gql from 'graphql-tag';

export const UPDATE_PROFILE = gql`
  mutation UpdateProfile (
    $first_name: String,
    $last_name: String,
    $role: ProfileRoleEnum
  ){
    UpdateProfile (
      input: {
        first_name: $first_name
        last_name: $last_name
        role: $role
      }
    ) {
      id
      family_id
      first_name
      last_name
      role
    }
  }
`;
