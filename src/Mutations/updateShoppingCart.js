import gql from 'graphql-tag';

export const UPDATE_SHOPPING_CART = gql`
  mutation UpdateShoppingCart (
    $id: String,
    $name: String,
    $created_at: Int,
    $created_by: String
  ){
    UpdateCart (
      input: {
        id: $id
        name: $name,
        created_at: $created_at
        created_by: $created_by
      }
    ) {
      result {
        id
        name
        created_at
        created_by
      }
    }
  }
`;
