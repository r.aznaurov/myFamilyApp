import gql from 'graphql-tag';

export const UPDATE_PRODUCT = gql`
  mutation UpdateProduct (
    $id: String,
    $cart_id: String,
    $name: String,
    $quantity: Int,
    $units: String,
    $type: String,
    $created_by: String,
    $created_at: Int,
    $updated_by: String,
    $updated_at: Int,
    $bought_by: String,
    $bought_at: Int
  ){
    UpdateProduct (
      input: {
        id: $id
        cart_id: $cart_id
        name: $name
        quantity: $quantity
        units: $units
        type: $type
        created_by: $created_by
        created_at: $created_at
        updated_by: $updated_by
        updated_at: $updated_at
        bought_by: $bought_by
        bought_at: $bought_at
      }
    ) {
      result {
        id
        cart_id
        name
        quantity
        units
        type
        created_by
        created_at
        updated_by
        updated_at
        bought_by
        bought_at
      }
    }
  }
`;


