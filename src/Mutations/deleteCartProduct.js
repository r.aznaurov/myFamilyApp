import gql from 'graphql-tag';

export const DELETE_CART_PRODUCT = gql`  
  mutation DeleteProduct (
    $id: String,
    $cart_id: String
  ){
    DeleteProduct (
      input: {
        id: $id
        cart_id: $cart_id
      }
    ) {
      result {
        id
        cart_id
      }
    }
  }
`;
