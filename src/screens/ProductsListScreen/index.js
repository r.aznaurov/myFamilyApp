import React from 'react';
import { ProductsList } from '../../modules/ProductsList';

export const ProductsListScreen = () => <ProductsList />;
