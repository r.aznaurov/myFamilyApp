import React from 'react';
import { Settings } from '../../modules/Settings';

export const SettingsScreen = () => <Settings />;
