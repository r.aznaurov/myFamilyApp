import React from 'react';
import { ShoppingCartList } from '../../modules/AppModules/ShoppingCartModule';

export const ShoppingCartListScreen = () => <ShoppingCartList />;
