export const HEADER_TYPES = {
  PRIMARY: 'primary',
  SECONDARY: 'secondary'
};

export const COLORS = {
  PRIMARY: '#529ff3',
  BLACK_ONE: '#565656',
  RED: '#f74131'
};

export const LOGO = require('../src/static/img/logo.png');

export const MODULES_IMG = {
  PRODUCTS_LIST: require('../src/static/img/modules/productslist.jpg'),
  WEEKEND_PLANS: require('../src/static/img/modules/weekendplans.jpg'),
  CALENDAR: require('../src/static/img/modules/calendar.jpg')
};

export const MODULES = {
  SHOPPING_CART: 'SHOPPING_CART',
  CALENDAR: 'CALENDAR',
  WEEKEND_PLANS: 'WEEKEND_PLANS'
};

export const CART_IMG = {
  FOOD: require('./static/img/shoppingcart/food.png'),
  ALCOHOL: require('./static/img/shoppingcart/alcohol.png'),
  SWEETS: require('./static/img/shoppingcart/sweets.png'),
  HOUSEHOLD: require('./static/img/shoppingcart/household.png')
};
