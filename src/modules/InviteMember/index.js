import React, { PureComponent } from 'react';
import { graphql, compose } from 'react-apollo';
import { View, AppState } from 'react-native';
import get from 'lodash.get';
import { Container, Content, Text, Form, Card, CardItem, Body } from 'native-base';
import { SubmitButton } from '../../components/SubmitButton';
import { CREATE_INVITE_CODE } from '../../Mutations/generateCode';
import { CountdownTimer } from '../../components/CountdownTimer';
import styles from './styles';

class InviteMemberComponent extends PureComponent {
  constructor() {
    super();

    this.clearInviteCodeAndTimer = this.clearInviteCodeAndTimer.bind(this);
  }
  state = {
    appState: AppState.currentState,
    inviteCode: null,
    submitProcessing: false,
    generateBtnActive: true
  };

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      this.clearInviteCodeAndTimer();
    }
    this.setState({appState: nextAppState});
  };

  onGenerateCode() {
    this.props.CreateInviteCode()
      .then(res => {
        const inviteCode = get(res.data, 'CreateInviteCode.result.invite_code') || '';

        this.setState({ inviteCode, generateBtnActive: false });
      })
      .catch(err => console.warn('ERROR', err));
  }

  clearInviteCodeAndTimer() {
    this.setState({ inviteCode: null, generateBtnActive: true });
  }

  render() {

    return (
      <Container>
        <Content padder>
          <Card transparent>
            <CardItem>
              <Body style={ styles.inviteCodeWrapper }>
                <Form>
                  <Text style={ styles.cardText }>
                  Code will appear here:
                  </Text>
                  <View style={ styles.inviteCodeHolder }>
                    <Text style={ styles.inviteCodeText }>
                      { this.state.inviteCode }
                    </Text>
                  </View>
                  <Text style={ styles.cardText }>
                  And it will be available only for 5 minutes
                  </Text>
                  <SubmitButton
                    onPress={ () => this.onGenerateCode() }
                    submitProcessing={ this.state.submitProcessing }
                    disabled={!this.state.generateBtnActive}
                  >
                  Generate Code
                  </SubmitButton>
                  {
                    this.state.inviteCode
                      ? <CountdownTimer
                        seconds={300}
                        onTimerEnd={() => this.clearInviteCodeAndTimer()}
                        timerContainerStyle={styles.timerContainer}
                        timerTextStyle={styles.timerText}
                      />
                      : null
                  }

                </Form>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

export const InviteMember = compose(
  graphql(CREATE_INVITE_CODE, {
    props: props => ({
      CreateInviteCode: user => props.mutate({
        // optimisticResponse: () => ({
        //   UpdateProfile: {
        //     id: null,
        //     family_id: null,
        //     first_name: user.firstName,
        //     last_name: user.lastName,
        //     role: user.role,
        //     __typename: 'ME'
        //   }
        // })
      })
    }),
    options: {
      // update: (dataProxy, { data: { updatePost } }) => {
      //   const query = GET_ME;
      //   const data = dataProxy.readQuery({ query });
      //
      //   data.allPost.posts = data.allPost.posts.map(post => post.id !== updatePost.id ? post : { ...updatePost });
      //
      //   dataProxy.writeQuery({ query, data });
      // }
    }
  })
)(InviteMemberComponent);
