import { StyleSheet } from 'react-native';
import { COLORS } from '../../constants';

export default StyleSheet.create({
  inviteCodeWrapper: {
    justifyContent: 'center'
  },
  inviteCodeHolder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 60
  },
  inviteCodeText: {
    textAlign: 'center',
    fontSize: 50,
    color: COLORS.PRIMARY
  },
  cardText: {
    textAlign: 'center',
    marginVertical: 20
  },
  timerContainer: {
    justifyContent: 'center'
  },
  timerText: {
    textAlign: 'center',
    color: COLORS.RED,
    paddingTop: 20
  }
});
