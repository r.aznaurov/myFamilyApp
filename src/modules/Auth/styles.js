import { StyleSheet } from 'react-native';
import { COLORS } from '../../constants';

export default StyleSheet.create({
  signInContent: {
    paddingTop: 10
  },
  logoHolderBig: {
    flex: 1,
    width: 120,
    height: 150,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  logoBig: {
    width: 120,
    height: 150
  },
  logoHolderSmall: {
    flex: 1,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 10
  },
  logoSmall: {
    width: 50,
    height: 50
  },
  signInForm: {
    paddingTop: 30
  },
  formItem: {
    marginBottom: 20
  },
  transparentLink: {
    alignSelf: 'center',
    marginVertical: 10
  },
  transparentLinkText: {
    textAlign: 'center',
    color: COLORS.PRIMARY
  },
  formText: {
    color: COLORS.BLACK_ONE
  },
  submitBtn: {
    flex: 1,
    marginTop: 30
  },
  cardInput: {
    marginTop: 30
  },
  screenHeading: {
    marginBottom: 30
  },
  errorText: {
    color: 'red',
    fontSize: 10,
    paddingLeft: 20
  }
});
