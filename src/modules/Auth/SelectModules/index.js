import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { graphql, compose } from 'react-apollo';
import { View, Image } from 'react-native';
import {
  Container,
  Text,
  Content,
  Card,
  CardItem,
  Body,
  Toast
} from 'native-base';
import { SubmitButton } from '../../../components/SubmitButton';
import { ModuleItem } from './ModuleItem';
import { LOGO, MODULES_IMG, MODULES } from '../../../constants';
import { CREATE_FAMILY } from '../../../Mutations/createFamily';
import { GET_ME } from '../../../Queries/getMEQuery';
import styles from './../styles';

const modulesData = [
  {
    id: MODULES.SHOPPING_CART,
    icon: 'cart',
    disabled: false,
    title: 'Shopping cart',
    image: MODULES_IMG.PRODUCTS_LIST,
    description: 'Забудьте про списки на бумажках! ' +
      'Этот модуль позволит создать список продуктов, ' +
      'который будет виден всем членам вашей семьи, ' +
      'чтобы все необходимое оказалось у вас на кухне!'
  },
  {
    id: MODULES.CALENDAR,
    icon: 'calendar',
    disabled: true,
    title: 'Calendar',
    image: MODULES_IMG.CALENDAR,
    description: 'Теперь все важные семейные события собраны в одном месте!'
  },
  {
    id: MODULES.WEEKEND_PLANS,
    icon: 'star',
    disabled: false,
    title: 'Weekend plans',
    image: MODULES_IMG.WEEKEND_PLANS,
    description: 'Как часто у вас выходило так, ' +
      'что в субботу утром вы обнаруживаете, ' +
      'что не знаете, чем заняться на выходных и ' +
      'просто тупо сидите дома? Теперь ваша проблема решена! ' +
      'Совместно наполняйте свой список планов в течение ' +
      'недели появившимися идеями и проводите выходные ярко!'
  }
];

class SelectModulesComponent extends Component {
  constructor() {
    super();

    this.onModuleSelect = this.onModuleSelect.bind(this);
  }

  state = {
    chosenModules: {},
    submitProcessing: false
  };

  onModuleSelect(id) {
    const chosenModules = this.state.chosenModules;

    chosenModules[id] = !chosenModules[id];
    this.setState({ chosenModules });
  }

  renderModuleItems() {
    return modulesData.map((module, i) => (
      <ModuleItem
        key={ `${module.title}${i}` }
        id={ module.id }
        icon={ module.icon }
        title={ module.title }
        image={ module.image }
        description={ module.description }
        disabled={ module.disabled }
        onModuleSelect={ this.onModuleSelect }
      />
    ));
  }

  getArrayOfChosenModules() {
    const chosenModules = this.state.chosenModules;

    return Object.keys(chosenModules).reduce((arr, item) => {
      chosenModules[item] && arr.push(item);

      return arr;
    }, []);
  }

  onSubmitModules() {
    const familyName = this.props.navigation.getParam('familyName');
    const modules = this.getArrayOfChosenModules();

    this.props.CreateFamily({ familyName, modules })
      .then(() => {
        this.setState({ submitProcessing: true });
        this.props.navigation.navigate('SplashScreen');
      })
      .catch(err => {
        this.setState({ submitProcessing: false });
        Toast.show({
          text: err.message,
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 7000
        });
      });
  }

  render() {
    return (
      <Container>
        <Content padder style={ styles.signInContent }>
          <View style={ styles.logoHolderSmall }>
            <Image
              style={ styles.logoSmall }
              source={ LOGO }
            />
          </View>
          <Card transparent>
            <CardItem>
              <Body>
                <Text>
                Ok! Now you have to choose, which modules you will use with your family.
                </Text>
                <Text>
                Note that you can always change your choice later in Settings.
                </Text>
              </Body>
            </CardItem>
          </Card>
          <View>
            { this.renderModuleItems() }
          </View>
          <SubmitButton
            onPress={ () => this.onSubmitModules() }
            submitProcessing={ this.state.submitProcessing }
          >
            Let's start
          </SubmitButton>
        </Content>
      </Container>
    );
  }
}

export const SelectModules = compose(
  graphql(CREATE_FAMILY, {
    props: props => ({
      CreateFamily: data => props.mutate({
        variables: {
          name: data.familyName,
          modules: data.modules
        }
        // optimisticResponse: () => ({
        //   UpdateProfile: {
        //     id: null,
        //     family_id: null,
        //     first_name: user.firstName,
        //     last_name: user.lastName,
        //     role: user.role,
        //     __typename: 'ME'
        //   }
        // })
      })
    }),
    options: {
      refetchQueries: [{ query: GET_ME }]
      // update: (dataProxy, { data: { updatePost } }) => {
      //   const query = GET_ME;
      //   const data = dataProxy.readQuery({ query });
      //
      //   data.allPost.posts = data.allPost.posts.map(post => post.id !== updatePost.id ? post : { ...updatePost });
      //
      //   dataProxy.writeQuery({ query, data });
      // }
    }
  }),
  withNavigation
)(SelectModulesComponent);
