import { StyleSheet } from 'react-native';
import { COLORS } from '../../../../constants';

export default StyleSheet.create({
  moduleCard: {
    flex: 0
  },
  disabledCardHeaderText: {
    fontSize: 14,
    color: COLORS.PRIMARY
  },
  moduleCardImage: {
    height: 200,
    width: null,
    flex: 1
  }
});
