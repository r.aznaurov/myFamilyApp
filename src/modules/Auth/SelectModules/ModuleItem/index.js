import React, { Component } from 'react';
import { Image } from 'react-native';
import {
  Text,
  Icon,
  Card,
  CardItem,
  Body,
  Left,
  Right,
  Switch
} from 'native-base';
import styles from '../../styles';
import moduleStyles from './styles';

export class ModuleItem extends Component {
  state = {
    switchValue: false
  };

  toggleSwitch = value => {
    const { onModuleSelect, id } = this.props;

    this.setState({ switchValue: value },
      onModuleSelect(id)
    );
  };

  render() {
    const { id, title, image, description, disabled, icon } = this.props;
    return (
      <Card style={ moduleStyles.moduleCard }>
        <CardItem>
          <Left>
            <Icon name={icon} style={ styles.categoryIcon }/>
            <Body>
              <Text>{ title }</Text>
            </Body>
          </Left>
          <Right>
            {
              !disabled
                ? <Switch
                  value={ this.state.switchValue }
                  onValueChange={ this.toggleSwitch }
                />
                : <Text style={ moduleStyles.disabledCardHeaderText }>PRO only</Text>
            }
          </Right>
        </CardItem>
        <CardItem cardBody>
          <Image source={ image } style={ moduleStyles.moduleCardImage }/>
        </CardItem>
        <CardItem>
          <Body>
            <Text>
              { description }
            </Text>
          </Body>
        </CardItem>
      </Card>
    );
  }
}
