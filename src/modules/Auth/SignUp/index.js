import React, { Component } from 'react';
import { Auth } from 'aws-amplify';
import { withApollo, compose } from 'react-apollo';
import {
  Container,
  Picker,
  Icon,
  Content,
  Form,
  Item,
  Button,
  Text,
  Toast,
  Spinner
} from 'native-base';
import { withNavigation } from 'react-navigation';
import { Image, View, AsyncStorage } from 'react-native';
import { storeDataToAsyncStorage } from '../../../helpers/helpers';
import { validate } from '../../../helpers/validateWrapper';
import { LOGO } from '../../../constants';
import styles from '../styles';
import { TextInput } from '../../../components/TextInput';
import { SubmitButton } from '../../../components/SubmitButton';

class SignUpComponent extends Component {

  state = {
    name: '',
    nameError: null,
    lastName: '',
    lastNameError: null,
    email: '',
    emailError: null,
    role: 'husband',
    password: '',
    passwordError: null,
    confirmPassword: '',
    confirmPasswordError: null,
    signUpError: null,
    submitProcessing: false
  };

  onValueChange(value : string) {
    this.setState({
      role: value
    });
  }

  SignUp() {
    const emailError = validate('email', this.state.email);
    const passwordError = validate('password', this.state.password);
    const confirmPasswordError = validate('confirmPassword', this.state.password, this.state.confirmPassword);

    this.setState(
      {
        emailError,
        passwordError,
        confirmPasswordError
      },
      () => {
        if (
          !emailError &&
          !passwordError &&
          !confirmPasswordError
        ) {
          Auth.signUp({
            username: this.state.email.toLowerCase(),
            password: this.state.password
          })
            .then(() => this.props.client.cache.reset())
            .then(() => {
              this.setState({submitProcessing: true});
              storeDataToAsyncStorage('user_email', this.state.email);
              this.props.navigation.navigate('SignUpConfirm', {
                username: this.state.email,
                password: this.state.password
              });
            })
            .catch(err => {
              this.setState({
                signUpError: this.getSignUpError(err.code),
                submitProcessing: false
              },
              () => {
                Toast.show({
                  text: this.state.signUpError,
                  buttonText: 'Ok',
                  position: 'bottom',
                  type: 'danger',
                  duration: 7000
                });
              });
            });
        }
      }
    );
  }

  getSignUpError(error) {
    switch (error) {
      case 'UsernameExistsException':
        return 'Account with the given email already exists!';
      default:
        return 'Something gone wrong';
    }
  }

  render() {
    return (
      <Container>
        <Content padder>
          <View style={ styles.logoHolderBig }>
            <Image
              style={ styles.logoBig }
              source={ LOGO }
            />
          </View>
          <Form style={ styles.signInForm }>
            <TextInput
              keyboardType={ 'email-address' }
              value={ this.state.email }
              onChangeText={ email => this.setState({ email }) }
              placeholder={ 'Your Email' }
              onBlur={ () => {
                this.setState({
                  emailError: validate('email', this.state.email)
                });
              } }
              error={ this.state.emailError }
            />
            <TextInput
              value={ this.state.password }
              onChangeText={ password => this.setState({ password }) }
              placeholder={ 'Password' }
              secureTextEntry={ true }
              onBlur={ () => {
                this.setState({
                  passwordError: validate('password', this.state.password)
                });
              } }
              error={ this.state.passwordError }
            />
            <TextInput
              value={ this.state.confirmPassword }
              onChangeText={ confirmPassword => this.setState({ confirmPassword }) }
              placeholder={ 'Confirm Password' }
              secureTextEntry={ true }
              onBlur={ () => {
                this.setState({
                  confirmPasswordError: validate('confirmPassword', this.state.password, this.state.confirmPassword)
                });
              } }
              error={ this.state.confirmPasswordError }
            />
            <SubmitButton
              onPress={ () => this.SignUp() }
              submitProcessing={this.state.submitProcessing}
            >
              Sign up!
            </SubmitButton>
          </Form>
          <Button
            transparent
            style={ styles.transparentLink }
            onPress={ () => this.props.navigation.navigate('SignIn') }
          >
            <Text style={ styles.transparentLinkText }>Sign in</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export const SignUp = compose(
  withNavigation,
  withApollo
)(SignUpComponent);
