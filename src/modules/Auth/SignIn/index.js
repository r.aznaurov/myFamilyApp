import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { View, Image } from 'react-native';
import { Container, Button, Text, Content, Form } from 'native-base';
import { Auth } from 'aws-amplify';
import { graphql, withApollo, compose } from 'react-apollo';
import { SubmitButton } from '../../../components/SubmitButton';
import { GET_ME } from '../../../Queries/getMEQuery';
import { TextInput } from '../../../components/TextInput';
import { LOGO } from '../../../constants';
import { validate } from '../../../helpers/validateWrapper';
import styles from './../styles';

class SignInComponent extends Component {
  state = {
    username: '',
    usernameError: null,
    password: '',
    passwordError: null,
    signInError: null,
    submitProcessing: false
  };

  SignIn() {
    const usernameError = validate('email', this.state.username);
    const passwordError = validate('password', this.state.password);

    this.setState({
      usernameError: usernameError,
      passwordError: passwordError
    });

    if (usernameError === null && passwordError === null) {
      this.setState({ submitProcessing: true});
      this.props.client.cache.reset().then(() =>
        Auth.signIn(
          this.state.username.toLowerCase(),
          this.state.password
        )
          .then(() => {
            console.warn('sign', new Date());

            this.props.navigation.navigate('SplashScreen');

          })
          .catch(err => {
            this.setState({
              signInError: this.getSignInError(err.code),
              submitProcessing: false
            },
            () => {
              if (err.code === 'UserNotConfirmedException') {
                this.props.navigation.navigate('SignUpConfirm', {
                  username: this.state.username,
                  password: this.state.password,
                  errorMessage: this.state.signInError
                });
              }
            });
          })
      );
    }
  }

  getSignInError(error) {
    switch (error) {
      case 'UserNotConfirmedException':
        return 'You did\'t confirm your registration! Check your email, and enter confirm code!';
      case 'NotAuthorizedException':
        return 'Incorrect email and password!';
      default:
        return 'Something gone wrong';
    }
  }

  render() {

    return (
      <Container>
        <Content padder style={ styles.signInContent }>
          <View style={ styles.logoHolderBig }>
            <Image
              style={ styles.logoBig }
              source={ LOGO }
            />
          </View>
          <Form style={ styles.signInForm }>
            <TextInput
              keyboardType={ 'email-address' }
              value={ this.props.navigation.getParam('username', this.state.username) }
              onChangeText={ username => this.setState({ username }) }
              placeholder={ 'Email' }
              onBlur={ () => {
                this.setState({
                  usernameError: validate('email', this.state.username)
                });
              } }
              error={ this.state.usernameError }
            />
            <TextInput
              value={ this.props.navigation.getParam('password', this.state.password) }
              onChangeText={ password => this.setState({ password }) }
              placeholder={ 'Password' }
              secureTextEntry={ true }
              onBlur={ () => {
                this.setState({
                  passwordError: validate('password', this.state.password)
                });
              } }
              error={ this.state.passwordError }
            />
            {
              this.state.signInError
                ? <Text style={ styles.errorText }>
                  { this.state.signInError }
                </Text>
                : null
            }
            <SubmitButton
              onPress={ () => this.SignIn() }
              submitProcessing={this.state.submitProcessing}
            >
              Sign in!
            </SubmitButton>
          </Form>
          <Button transparent style={ styles.transparentLink }>
            <Text style={ styles.transparentLinkText }>Forgot password?</Text>
          </Button>
          <Text style={ [styles.transparentLinkText, styles.formText] }>Don't have account?</Text>
          <Button
            transparent
            style={ styles.transparentLink }
            onPress={ () => this.props.navigation.navigate('SignUp') }
          >
            <Text style={ styles.transparentLinkText }>Sign up!</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export const SignIn = compose(
  graphql(GET_ME, {
    options: { fetchPolicy: 'cache-and-network' }
  }),
  withApollo,
  withNavigation
)(SignInComponent);
