import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { UPDATE_PROFILE } from '../../../Mutations/updateProfile';
import { GET_ME } from '../../../Queries/getMEQuery';
import {
  Container,
  Picker,
  Icon,
  Content,
  Form,
  Item,
  Text,
  Toast
} from 'native-base';
import { withNavigation } from 'react-navigation';
import { Image, View } from 'react-native';
import { validate } from '../../../helpers/validateWrapper';
import { LOGO } from '../../../constants';
import styles from '../styles';
import { TextInput } from '../../../components/TextInput';
import { SubmitButton } from '../../../components/SubmitButton';

class SetProfileComponent extends Component {

  state = {
    firstName: '',
    nameError: null,
    lastName: '',
    lastNameError: null,
    role: 'null',
    createProfileError: null,
    submitProcessing: false
  };

  onValueChange(value : string) {
    this.setState({
      role: value
    });
  }

  onProfileSubmit() {
    const nameError = validate('name', this.state.firstName);
    const lastNameError = validate('name', this.state.lastName);

    this.setState(
      {
        nameError,
        lastNameError
      },
      () => {
        if (!nameError && !lastNameError) {
          this.setState({submitProcessing: true});
          this.props.UpdateProfile({
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            role: this.state.role
          })
            .then(() => {
              this.props.navigation.navigate('SplashScreen');
            })
            .catch(err => {
              this.setState({submitProcessing: false});
              Toast.show({
                text: err.message,
                buttonText: 'Okay',
                position: 'bottom',
                type: 'danger',
                duration: 6000
              });
            });
        }
      }
    );
  }

  render() {
    return (
      <Container>
        <Content padder>
          <View style={ styles.logoHolderBig }>
            <Image
              style={ styles.logoBig }
              source={ LOGO }
            />
          </View>
          <Form style={ styles.signInForm }>
            <Text style={ styles.screenHeading }>
              Some information about you to your profile!
            </Text>
            <TextInput
              value={ this.state.firstName }
              onChangeText={ firstName => this.setState({ firstName }) }
              placeholder={ 'Your Name' }
              onBlur={ () => {
                this.setState({
                  nameError: validate('name', this.state.firstName)
                });
              } }
              error={ this.state.nameError }
            />
            <TextInput
              value={ this.state.lastName }
              onChangeText={ lastName => this.setState({ lastName }) }
              placeholder={ 'Your Last Name' }
              onBlur={ () => {
                this.setState({
                  lastNameError: validate('lastName', this.state.lastName)
                });
              } }
              error={ this.state.lastNameError }
            />
            <Item rounded style={ styles.formItem }>
              <Picker
                placeholder={ 'Select your role' }
                mode="dropdown"
                iosHeader="Select your role"
                iosIcon={ <Icon name="ios-arrow-down-outline"/> }
                selectedValue={ this.state.role }
                onValueChange={ this.onValueChange.bind(this) }
              >
                <Picker.Item label="Select your role" value="null"/>
                <Picker.Item label="Husband" value="husband"/>
                <Picker.Item label="Wife" value="wife"/>
                <Picker.Item label="Partner" value="partner"/>
                <Picker.Item label="Son" value="son"/>
                <Picker.Item label="Daughter" value="daughter"/>
              </Picker>
            </Item>
            <SubmitButton
              onPress={ () => this.onProfileSubmit() }
              submitProcessing={this.state.submitProcessing}
            >
              Save
            </SubmitButton>
          </Form>
        </Content>
      </Container>
    );
  }
}

export const SetProfile = compose(
  graphql(UPDATE_PROFILE, {
    props: props => ({
      UpdateProfile: user => props.mutate({
        variables: {
          first_name: user.firstName,
          last_name: user.lastName,
          role: user.role
        }
        // optimisticResponse: () => ({
        //   UpdateProfile: {
        //     id: null,
        //     family_id: null,
        //     first_name: user.firstName,
        //     last_name: user.lastName,
        //     role: user.role,
        //     __typename: 'ME'
        //   }
        // })
      })
    }),
    options: {
      refetchQueries: [{ query: GET_ME }]
      // update: (dataProxy, { data: { updatePost } }) => {
      //   const query = GET_ME;
      //   const data = dataProxy.readQuery({ query });
      //
      //   data.allPost.posts = data.allPost.posts.map(post => post.id !== updatePost.id ? post : { ...updatePost });
      //
      //   dataProxy.writeQuery({ query, data });
      // }
    }
  }),
  withNavigation
)(SetProfileComponent);
