import React, { Component } from 'react';
import { Auth } from 'aws-amplify';
import {
  Container,
  Content,
  Button,
  Text,
  Form,
  Toast,
  Spinner
} from 'native-base';
import { withNavigation } from 'react-navigation';
import { Image, View, AsyncStorage } from 'react-native';
import { LOGO } from '../../../constants';
import { validate } from '../../../helpers/validateWrapper';
import { TextInput } from '../../../components/TextInput';
import styles from '../styles';
import { SubmitButton } from '../../../components/SubmitButton';

class SignUpConfirmComponent extends Component {

  state = {
    confirmCode: null,
    confirmCodeError: null,
    submitProcessing: false
  };

  confirmSignUp() {
    const username = this.props.navigation.getParam('username').toLowerCase();
    const password = this.props.navigation.getParam('password');
    const confirmCodeError = validate('confirmCode', this.state.confirmCode);

    this.setState(
      { confirmCodeError },
      () => {
        if (!confirmCodeError) {
          Auth.confirmSignUp(username, this.state.confirmCode)
            .then(res => {
              this.setState({submitProcessing: true});
              Auth.signIn(username, password)
                .then(res => {
                  this.props.navigation.navigate('SetProfile');
                })
                .catch(err => {
                  this.setState({submitProcessing: false});
                  Toast.show({
                    text: err.message,
                    buttonText: 'Okay',
                    position: 'bottom',
                    type: 'danger',
                    duration: 7000
                  });
                });
            })
            .catch(err => {
              this.setState({
                confirmCode: null,
                submitProcessing: false
              });

              Toast.show({
                text: err.message,
                buttonText: 'Okay',
                position: 'bottom',
                type: 'danger',
                duration: 7000
              });
            });
        }
      }
    );
  }

  render() {
    const errorMessage = this.props.navigation.getParam('errorMessage', null);

    return (
      <Container>
        <Content padder style={ styles.signInContent }>
          <View style={ styles.logoHolderBig }>
            <Image
              style={ styles.logoBig }
              source={ LOGO }
            />
          </View>
          <Form style={ styles.signInForm }>
            <Text style={ styles.screenHeading }>
              Check your email box for confirmation code! And enter it to input!
            </Text>
            <TextInput
              textStyle={ { textAlign: 'center' } }
              value={ this.state.confirmCode }
              onChangeText={ confirmCode => this.setState({ confirmCode }) }
              placeholder={ 'XXXXXX' }
              onBlur={ () => {
                this.setState({
                  confirmCodeError: validate('confirmCode', this.state.confirmCode)
                });
              } }
              error={ this.state.confirmCodeError }
            />
            {
              errorMessage
                ? <Text style={ styles.errorText }>
                  { errorMessage }
                </Text>
                : null
            }
          </Form>
          <SubmitButton
            onPress={ () => this.confirmSignUp() }
            submitProcessing={this.state.submitProcessing}
          >
            Confirm
          </SubmitButton>
        </Content>
      </Container>
    );
  }
}

export const SignUpConfirm = withNavigation(SignUpConfirmComponent);
