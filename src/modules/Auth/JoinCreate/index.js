import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { withApollo, graphql, compose } from 'react-apollo';
import { View, Image } from 'react-native';
import { Container, Button, Text, Content, Form, Item, Input, Card, CardItem, Body } from 'native-base';
import { LOGO } from '../../../constants';
import styles from './../styles';
import { validate } from '../../../helpers/validateWrapper';
import { TextInput } from '../../../components/TextInput';
import { SubmitButton } from '../../../components/SubmitButton';
import { ACTIVATE_INVITE_CODE } from '../../../Mutations/activateInviteCode';
import { GET_ME } from '../../../Queries/getMEQuery';

class JoinCreateComponent extends Component {
  state = {
    inviteCode: null,
    inviteCodeError: null,
    submitProcessing: false
  };

  goToCreateAccount() {
    this.props.navigation.navigate('CreateAccount');
  }

  onJoinFamily() {
    const inviteCodeError = validate('inviteCode', this.state.inviteCode);

    this.setState(
      {
        inviteCodeError,
        submitProcessing: true
      },
      () => {
        if (!inviteCodeError) {
          this.props.JoinFamily({inviteCode: this.state.inviteCode})
            .then(res => {
              this.props.navigation.navigate('SplashScreen');
            })
            .catch(err => {
              console.warn('Cancel joining', err); // TODO: Здесь обработать ошибки и придумать приколы на ошибку каждую
            });
        }
      }
    );
  }

  render() {

    return (
      <Container>
        <Content padder style={ styles.signInContent }>
          <View style={ styles.logoHolderBig }>
            <Image
              style={ styles.logoBig }
              source={ LOGO }
            />
          </View>
          <Card transparent>
            <CardItem>
              <Body>
                <Text style={ [styles.transparentLinkText, styles.formText] }>
                  If you are creating new account press:
                </Text>
                <Button
                  block
                  rounded
                  info
                  style={ styles.submitBtn }
                  onPress={ () => this.goToCreateAccount() }
                >
                  <Text>Create Family Account</Text>
                </Button>
              </Body>
            </CardItem>
          </Card>
          <Text style={ [styles.transparentLinkText, styles.formText] }>
            Or ask your family member for invite code and enter it here:
          </Text>
          <Form>
            <TextInput
              textStyle={ { textAlign: 'center' } }
              value={ this.state.inviteCode }
              onChangeText={ inviteCode => this.setState({ inviteCode }) }
              placeholder={ 'XXXXXX' }
              onBlur={ () => {
                this.setState({
                  inviteCodeError: validate('inviteCode', this.state.inviteCode)
                });
              } }
              error={ this.state.inviteCodeError }
            />
          </Form>

          <SubmitButton
            onPress={ () => this.onJoinFamily() }
            submitProcessing={this.state.submitProcessing}
          >
            Join my Family
          </SubmitButton>

        </Content>
      </Container>
    );
  }
}

export const JoinCreate = compose(
  graphql(ACTIVATE_INVITE_CODE, {
    props: props => ({
      JoinFamily: data => props.mutate({
        variables: {
          invite_code: data.inviteCode
        }
        // optimisticResponse: () => ({
        //   UpdateProfile: {
        //     id: null,
        //     family_id: null,
        //     first_name: user.firstName,
        //     last_name: user.lastName,
        //     role: user.role,
        //     __typename: 'ME'
        //   }
        // })
      })
    }),
    options: {
      refetchQueries: [{ query: GET_ME }]
      // update: (dataProxy, { data: { updatePost } }) => {
      //   const query = GET_ME;
      //   const data = dataProxy.readQuery({ query });
      //
      //   data.allPost.posts = data.allPost.posts.map(post => post.id !== updatePost.id ? post : { ...updatePost });
      //
      //   dataProxy.writeQuery({ query, data });
      // }
    }
  }),
  withNavigation
)(JoinCreateComponent);
