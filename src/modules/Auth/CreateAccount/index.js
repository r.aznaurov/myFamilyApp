import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { View, Image } from 'react-native';
import { Container, Button, Text, Content, Item, Input, Form } from 'native-base';
import { LOGO } from '../../../constants';
import styles from './../styles';
import { TextInput } from '../../../components/TextInput';
import { validate } from '../../../helpers/validateWrapper';
import { SubmitButton } from '../../../components/SubmitButton';

class CreateAccountComponent extends Component {
  state = {
    familyName: null,
    familyNameError: null,
    submitProcessing: false
  };

  onCreateFamilyAccount() {
    const familyNameError = validate('name', this.state.familyName);

    this.setState(
      {
        familyNameError,
        submitProcessing: true
      },
      () => {
        if (!familyNameError) {
          this.props.navigation.navigate('SelectModules', {familyName: this.state.familyName});
        }
      }
    );
  }

  render() {
    return (
      <Container>
        <Content padder style={ styles.signInContent }>
          <View style={ styles.logoHolderBig }>
            <Image
              style={ styles.logoBig }
              source={ LOGO }
            />
          </View>
          <Text style={ styles.screenHeading }>
            Great! Let's start with your Family Name:
          </Text>
          <Form>
            <TextInput
              value={ this.state.familyName }
              onChangeText={ familyName => this.setState({ familyName }) }
              placeholder={ 'Your Family Name' }
              onBlur={ () => {
                this.setState({
                  familyNameError: validate('name', this.state.familyName)
                });
              } }
              error={ this.state.familyNameError }
            />
          </Form>
          <SubmitButton
            onPress={ () => this.onCreateFamilyAccount() }
            submitProcessing={this.state.submitProcessing}
          >
            Create Family
          </SubmitButton>
        </Content>
      </Container>
    );
  }
}

export const CreateAccount = withNavigation(CreateAccountComponent);
