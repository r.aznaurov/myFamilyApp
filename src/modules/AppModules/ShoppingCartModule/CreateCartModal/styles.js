import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  modalWrapper: {
    position: 'relative',
    paddingBottom: 60
  },
  modalCard: {
    paddingTop: 40,
    paddingRight: 10,
    paddingLeft: 10,
    height: 300
  },
  inputText: {
    textAlign: 'left',
    flex: 1
  },
  modalHeadingText: {
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 30
  },
  buttonsWrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'column'
  },
  submitButton: {
    marginBottom: 15,
    marginTop: 0
  },
  closeButton: {
    flex: 1
  }
});
