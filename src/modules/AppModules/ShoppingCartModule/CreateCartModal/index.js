import React, { Component } from 'react';
import { View } from 'react-native';
import { Text, Card, Button } from 'native-base';
import Modal from 'react-native-modal';
import { TextInput } from '../../../../components/TextInput';
import { SubmitButton } from '../../../../components/SubmitButton';
import styles from './styles';

export class CreateCartModal extends Component {
  render() {
    return (
      <Modal
        isVisible={ this.props.isVisible }
        onSwipe={ () => this.props.onClose() }
        swipeDirection='up'
        avoidKeyboard={ true }
        onBackButtonPress={ () => this.props.onClose() }
        onBackdropPress={ () => this.props.onClose() }
      >
        <Card padder rounded style={ styles.modalCard }>
          <Text style={ styles.modalHeadingText }>
            Create New Shopping Cart
          </Text>
          <TextInput
            textStyle={ styles.inputText }
            value={ this.props.cartName }
            onChangeText={ cartName => this.props.onNameChange(cartName) }
            placeholder={ 'Input new cart name' }
            error={ null }
          />
          <View style={ styles.buttonsWrapper }>
            <SubmitButton
              customStyle={ styles.submitButton }
              onPress={ () => this.props.onSubmit() }
              disabled={ this.props.cartName === ''}
            >
              Create New Cart
            </SubmitButton>
            <Button
              danger
              rounded
              onPress={ () => this.props.onClose() }
              style={ styles.closeButton }
            >
              <Text>Cancel</Text>
            </Button>
          </View>
        </Card>
      </Modal>
    );
  }
}

