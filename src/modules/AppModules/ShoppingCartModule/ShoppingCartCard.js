import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { graphql, compose } from 'react-apollo';
import { withNavigation } from 'react-navigation';
import { Container, Content, Card, CardItem, Text, Button, Icon } from 'native-base';
import { distanceInWordsToNow } from 'date-fns';
import * as Animatable from 'react-native-animatable';
import { RemoveCartModal } from './RemoveCartModal';
import { COLORS } from '../../../constants';

export class ShoppingCartCardComponent extends Component {
  constructor() {
    super();
    this.onCloseRemoveModal = this.onCloseRemoveModal.bind(this);
  }

  state = {
    removeModalVisible: false
  };

  onCloseRemoveModal() {
    this.setState({removeModalVisible: false});
  }

  render() {
    const { cartId, title, onDelete, createdAt, createdBy } = this.props;

    return (
      <Animatable.View animation="flipInY" direction="alternate">
        <RemoveCartModal
          cartName={ title }
          cartId={ cartId }
          onClose={ this.onCloseRemoveModal }
          isVisible={ this.state.removeModalVisible }
          onDelete={ onDelete }
        />
        <TouchableOpacity
          onPress={ () => this.props.navigation.navigate('ShoppingCart', { title, cartId }) }
        >
          <Card>
            <CardItem header bordered>
              <View
                style={ { flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' } }>
                <Icon name={ 'cart' }/>
                <Text>{ title }</Text>
                <Button
                  rounded
                  transparent
                  onPress={ () => this.setState({removeModalVisible: true}) }
                >
                  <Icon name='trash' style={ { color: COLORS.RED } }/>
                </Button>
              </View>
            </CardItem>
            <CardItem bordered>
              <View style={ { flex: 1, justifyContent: 'center', alignItems: 'center'} }>
                <Text style={ { alignSelf: 'flex-end', marginTop: 20, color: '#c8c8c8', fontSize: 10  } }>
                  created {distanceInWordsToNow(createdAt * 1000)} ago
                </Text>
                <Text style={ { alignSelf: 'flex-end' } }>
                  by { createdBy }
                </Text>
              </View>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </Animatable.View>
    );
  }
}

export const ShoppingCartCard = compose(
  withNavigation
)(ShoppingCartCardComponent);
