import React, { Component } from 'react';
import { View } from 'react-native';
import { Text, Card, Button } from 'native-base';
import Modal from 'react-native-modal';
import { SubmitButton } from '../../../../components/SubmitButton';
import styles from './styles';

export class RemoveCartModal extends Component {
  render() {
    const { cartName, cartId, onClose, isVisible, onDelete } = this.props;
    return (
      <Modal
        isVisible={ isVisible }
        onSwipe={ () => onClose() }
        swipeDirection='up'
        avoidKeyboard={ true }
        onBackButtonPress={ () => onClose() }
        onBackdropPress={ () => onClose() }
      >
        <Card padder rounded style={ styles.modalCard }>
          <Text style={ styles.modalHeadingText }>
            Do you really want to delete {cartName}?
          </Text>
          <View style={ styles.buttonsWrapper }>
            <SubmitButton
              customStyle={ styles.submitButton }
              onPress={ () => onDelete(cartId) }
            >
              Delete
            </SubmitButton>
            <Button
              danger
              rounded
              onPress={ () => onClose() }
              style={ styles.closeButton }
            >
              <Text>Cancel</Text>
            </Button>
          </View>
        </Card>
      </Modal>
    );
  }
}

