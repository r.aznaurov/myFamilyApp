import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import uuid from 'react-native-uuid';
import { graphql, compose, withApollo } from 'react-apollo';
import { withNavigation } from 'react-navigation';
import { Container, Content, Text, Icon } from 'native-base';
import * as Animatable from 'react-native-animatable';
import get from 'lodash.get';
import { CreateCartModal } from './CreateCartModal';
import { ShoppingCartCard } from './ShoppingCartCard';
import { UPDATE_SHOPPING_CART } from '../../../Mutations/updateShoppingCart';
import { GET_SHOPPING_CART_LIST } from '../../../Queries/getShoppingCartList';
import { DELETE_SHOPPING_CART } from '../../../Mutations/deleteShoppingCart';
import { GET_ME } from '../../../Queries/getMEQuery';
import { GET_FAMILY_MEMBERS } from '../../../Queries/getFamilyMembers';

export class ShoppingCartListComponent extends PureComponent {
  constructor() {
    super();

    this.onNameChange = this.onNameChange.bind(this);
    this.closeCreateCartModal = this.closeCreateCartModal.bind(this);
    this.onSubmitNewShoppingCart = this.onSubmitNewShoppingCart.bind(this);
    this.onDeleteShoppingCart = this.onDeleteShoppingCart.bind(this);
  }

  state = {
    isCreateCartModalVisible: false,
    newCartName: '',
    cartNameError: null
  };

  closeCreateCartModal() {
    this.setState({
      isCreateCartModalVisible: false,
      newCartName: ''
    });
  }

  onNameChange(newCartName) {
    this.setState({ newCartName });
  }

  showCreateCartModal() {
    this.setState({ isCreateCartModalVisible: true });
  }

  onSubmitNewShoppingCart() {
    const ME = this.props.client.readQuery({ query: GET_ME });
    const created_by = ME.Me.id;
    const id = uuid.v4();
    const name = this.state.newCartName;
    const created_at = +new Date() / 1000 | 0;

    this.props.UpdateShoppingCart({ id, name, created_by, created_at }).then(() => {})
      .catch(err => console.warn('CreateNewCartError', err));
    this.closeCreateCartModal();
  }

  onDeleteShoppingCart(id) {
    this.props.DeleteShoppingCart({ id });
  }

  getNameById(id) {
    const familyMembers = this.props.client.readQuery({query: GET_FAMILY_MEMBERS});
    const matchedUser = familyMembers.ListFamilyMembers.result.find(user => user.id === id) || {};

    return matchedUser.first_name || 'Unknown duck';
  }

  renderShoppingCarts() {
    const { ShoppingCartList } = this.props;
    const carts = get(ShoppingCartList, 'ListCarts.result') || [];

    return carts.map((cart, i) => (
      <ShoppingCartCard
        key={`${i}${cart.name}`}
        title={ cart.name }
        cartId={ cart.id }
        createdBy={ this.getNameById(cart.created_by) }
        createdAt={ cart.created_at }
        onDelete={ this.onDeleteShoppingCart }
      />
    ));
  }

  render() {
    return (
      <Container>
        <CreateCartModal
          isVisible={ this.state.isCreateCartModalVisible }
          onClose={ this.closeCreateCartModal }
          onNameChange={ this.onNameChange }
          cartName={ this.state.newCartName }
          onSubmit={ this.onSubmitNewShoppingCart }
        />
        <Content padder>
          { this.renderShoppingCarts() }
        </Content>
        <Animatable.View animation="bounceIn" direction="alternate" style={ {
          position: 'absolute',
          flexDirection: 'row',
          right: 20,
          bottom: 20,
          justifyContent: 'center',
          alignItems: 'center'
        } }>
          <TouchableOpacity
            style={ {
              backgroundColor: '#ead40e',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              width: 70,
              height: 70,
              borderRadius: 70
            } }
            onPress={ () => this.showCreateCartModal() }
          >
            <Text>+</Text>
            <Icon name='cart' style={ { color: '#000' } }/>
          </TouchableOpacity>
        </Animatable.View>
      </Container>
    );
  }
}

export const ShoppingCartList = compose(
  withApollo,
  graphql(GET_SHOPPING_CART_LIST,
    {
      name: 'ShoppingCartList',
      options: {
        fetchPolicy: 'cache-and-network'
      }
    }),
  graphql(UPDATE_SHOPPING_CART, {
    props: props => ({
      UpdateShoppingCart: data => props.mutate({
        variables: {
          id: data.id,
          name: data.name,
          created_at: data.created_at,
          created_by: data.created_by
        },
        optimisticResponse: () => ({
          UpdateCart: {
            result: {
              id: data.id,
              created_at: data.created_at,
              created_by: data.created_by,
              name: data.name,
              __typename: 'Cart'
            },
            __typename: 'CheckListCarts'
          }
        })
      })
    }),
    options: {
      fetchPolicy: 'cache-and-network',
      refetchQueries: [{ query: GET_SHOPPING_CART_LIST }],
      update: (dataProxy, { data: { UpdateCart } }) => {
        const query = GET_SHOPPING_CART_LIST;
        const data = dataProxy.readQuery({ query });
        const cart = data.ListCarts.result.find(item => item.id === UpdateCart.result.id);

        if (cart) {
          Object.keys(UpdateCart.result).forEach(key => {
            if (UpdateCart.result[key]) {
              cart[key] = UpdateCart.result[key];
            }
          });

          data.ListCarts.result = data.ListCarts.result.filter(item => item.id !== UpdateCart.result.id);
          data.ListCarts.result.push(cart);
          dataProxy.writeQuery({ query, data });
        } else {
          data.ListCarts.result.push(UpdateCart.result);
          dataProxy.writeQuery({ query, data });
        }
      }
    }
  }
  ),
  graphql(GET_SHOPPING_CART_LIST,
    {
      name: 'ShoppingCartList',
      options: {
        fetchPolicy: 'cache-and-network'
      }
    }),
  graphql(DELETE_SHOPPING_CART, {
    props: props => ({
      DeleteShoppingCart: data => props.mutate({
        variables: {
          id: data.id
        },
        optimisticResponse: () => ({
          DeleteCart: {
            result: {
              id: data.id,
              __typename: 'Cart'
            },
            __typename: 'CheckListCarts'
          }
        })
      })
    }),
    options: {
      fetchPolicy: 'cache-and-network',
      refetchQueries: [{ query: GET_SHOPPING_CART_LIST }],
      update: (dataProxy, { data: { DeleteCart } }) => {
        const query = GET_SHOPPING_CART_LIST;
        const data = dataProxy.readQuery({ query });

        data.ListCarts.result = data.ListCarts.result.filter(cart => cart.id !== DeleteCart.result.id);
        dataProxy.writeQuery({ query, data });
      }
    }
  }
  ),
  withNavigation
)(ShoppingCartListComponent);
