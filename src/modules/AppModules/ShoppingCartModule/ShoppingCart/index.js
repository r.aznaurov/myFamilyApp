import React, { PureComponent } from 'react';
import { withNavigation } from 'react-navigation';
import { compose, graphql, withApollo } from 'react-apollo';
import { Image, Keyboard } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Container, Content, View, Text, Button, Item, Input, Icon } from 'native-base';
import { ShoppingCartItem } from './ShoppingCartItem';
import { GET_PRODUCTS_LIST } from '../../../../Queries/getProductsList';
import { UPDATE_PRODUCT } from '../../../../Mutations/updateProduct';
import { DELETE_CART_PRODUCT } from '../../../../Mutations/deleteCartProduct';
import { GET_ME } from '../../../../Queries/getMEQuery';
import get from 'lodash.get';
import { CART_IMG } from '../../../../constants';

class ShoppingCartComponent extends PureComponent {
  constructor() {
    super();

    this.onDeleteProduct = this.onDeleteProduct.bind(this);
    this.onBuyProduct = this.onBuyProduct.bind(this);
    this.onAddProduct = this.onAddProduct.bind(this);
    this.onOpenOptions = this.onOpenOptions.bind(this);
  }

  state = {
    productName: '',
    productType: '',
    optionsVisible: false
  };

  onAddProduct(type) {
    const cartId = this.props.navigation.getParam('cartId');
    const createdAt = +(new Date()) / 1000 | 0;
    const id = `product${createdAt}`;

    Keyboard.dismiss();
    this.setState({optionsVisible: false, productType: type},
      () => {
        this.props.updateProduct({
          id: id,
          cart_id: cartId,
          name: this.state.productName,
          quantity: null,
          units: null,
          type: this.state.productType,
          created_by: null,
          created_at: createdAt,
          updated_by: null,
          updated_at: null,
          bought_by: null,
          bought_at: null
        }).then(res => {})
        .catch(err => console.warn('prod error', err));
        this.setState({productName: ''});
      }
    );

    // this.setState({ productName: '', optionsVisible: false });
  }

  onBuyProduct(id, name, quantity, units, type, created_by, created_at, updated_by, updated_at, bought_at, bought_by ) {
    const cartId = this.props.navigation.getParam('cartId');
    const ME = this.props.client.readQuery({ query: GET_ME });
    let boughtBy;
    let boughtAt;

    if ( !bought_at && !bought_by ) {
      boughtAt = +(new Date()) / 1000 | 0;
      boughtBy = ME.Me.id;
    } else {
      boughtAt = null;
      boughtBy = null;
    }

    this.props.updateProduct({
      id: id,
      cart_id: cartId,
      name: name,
      quantity: quantity,
      units: units,
      type: type,
      created_by: created_by,
      created_at: created_at,
      updated_by: updated_by,
      updated_at: updated_at,
      bought_by: boughtBy,
      bought_at: boughtAt
    })
      .then(res => {})
      .catch(err => console.warn('prod error', err));
  }

  onDeleteProduct(id) {
    const cartId = this.props.navigation.getParam('cartId');

    this.props.DeleteProduct({ id, cart_id: cartId });
  }

  onOpenOptions() {
    this.setState({optionsVisible: !this.state.optionsVisible});
  }

  sortArr(arr, sortBy) {
    return arr.sort((a, b) => {
      if(a[sortBy] < b[sortBy]) { return -1; }
      if(a[sortBy] > b[sortBy]) { return 1; }

      return 0;
    });
  }

  sortProducts(arr) {
    const boughtProducts = [];
    const notBoughtProducts = [];

    this.sortArr(arr, 'name')
      .forEach(item => {
        if (item.bought_at === null) {
          notBoughtProducts.push(item);
        } else {
          boughtProducts.push(item);
        }
      });

    const boughtAndSortedByType = this.sortArr(boughtProducts, 'type');
    const notBoughtAndSortedByType = this.sortArr(notBoughtProducts, 'type');

    return [...notBoughtAndSortedByType, ...boughtAndSortedByType];
  }

  renderProductItems() {
    const { ProductList } = this.props;
    const products = get(ProductList, 'ListCartProducts.result') || [];
    const sortedProducts = this.sortProducts(products);

    return sortedProducts.map((product, i) => (
      <ShoppingCartItem
        key={ `${i}${product.name}` }
        id={ product.id }
        name={ product.name }
        type={ product.type }
        quantity={product.quantity}
        units={product.units}
        created_by={product.created_by}
        created_at={product.created_at}
        bought_by={product.bought_by}
        bought_at={product.bought_at}
        onDelete={ this.onDeleteProduct }
        onBuy={ this.onBuyProduct }
      />
    ));
  }

  render() {
    return (
      <Container>
        <Content padder>
          <Text style={ { textAlign: 'center', fontSize: 20, marginBottom: 30 } }>
            { this.props.navigation.getParam('title') }
          </Text>
          <View
            style={ { flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', marginBottom: 30, position: 'relative' } }>
            <Item style={ { flex: 1, flexDirection: 'row', marginRight: 10 } }>
              <Input
                style={ { textAlign: 'left', flex: 1 } }
                value={ this.state.productName }
                onChangeText={ productName => this.setState({ productName, optionsVisible: true }) }
                placeholder={ 'Start to enter new product' }
              />
            </Item>
            {
              this.state.optionsVisible
            && <Animatable.View animation="flipInX" style={ { flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap' } }>
              <Button
                small
                info
                style={ { width: 50, height: 50, alignSelf: 'center', justifyContent: 'center', backgroundColor: '#e7fcb5', marginRight: 3, marginBottom: 3 } }
                onPress={ () => this.onAddProduct('food') }
              >
                <Image style={{ width: 40, height: 40 }} source={CART_IMG.FOOD}/>
              </Button>
              <Button
                small
                info
                style={ { width: 50, height: 50, justifyContent: 'center', alignSelf: 'center', backgroundColor: '#fcbfec', marginRight: 3, marginBottom: 3 } }
                onPress={ () => this.onAddProduct('alcohol') }
              >
                <Image style={{ width: 40, height: 40 }} source={CART_IMG.ALCOHOL}/>
              </Button>
              <Button
                small
                info
                style={ { width: 50, height: 50, alignSelf: 'center', justifyContent: 'center', backgroundColor: '#e994ef', marginRight: 3, marginBottom: 3 } }
                onPress={ () => this.onAddProduct('sweets') }
              >
                <Image style={{ width: 40, height: 40 }} source={CART_IMG.SWEETS}/>
              </Button>
              <Button
                small
                info
                style={ { width: 50, height: 50, alignSelf: 'center', justifyContent: 'center', backgroundColor: '#bfd7fc', marginRight: 3, marginBottom: 3 } }
                onPress={ () => this.onAddProduct('household') }
              >
                <Image style={{ width: 40, height: 40 }} source={CART_IMG.HOUSEHOLD}/>
              </Button>
            </Animatable.View>
            }
          </View>
          <View>
            {
              this.renderProductItems()
            }
          </View>
        </Content>
      </Container>
    );
  }
}

export const ShoppingCart = compose(
  withNavigation,
  withApollo,
  graphql(GET_PRODUCTS_LIST, {
    name: 'ProductList',
    options: ownProps => ({
      fetchPolicy: 'cache-and-network',
      variables: { cart_id: ownProps.navigation.getParam('cartId') }
    })
  }),
  graphql(UPDATE_PRODUCT, {
    props: (props, ownProps) => ({
      updateProduct: data => props.mutate({
        variables: {
          id: data.id,
          cart_id: data.cart_id,
          name: data.name,
          quantity: null,
          units: null,
          type: data.type,
          created_by: data.created_by,
          created_at: data.created_at,
          updated_by: null,
          updated_at: null,
          bought_by: data.bought_by,
          bought_at: data.bought_at
        },
        optimisticResponse: () => ({
          UpdateProduct: {
            result: {
              id: data.id,
              cart_id: data.cart_id,
              name: data.name || null,
              quantity: null,
              units: null,
              type: data.type || null,
              created_by: data.created_by || null,
              created_at: data.created_at || null,
              updated_by: null,
              updated_at: null,
              bought_by: data.bought_by || null,
              bought_at: data.bought_at || null,
              __typename: 'Product'
            },
            __typename: 'CheckListCartProducts'
          }
        })
      })
    }),
    options: {
      fetchPolicy: 'cache-and-network',
      update: (dataProxy, { data: { UpdateProduct } }) => {
        const query = GET_PRODUCTS_LIST;
        const variables = { cart_id: UpdateProduct.result.cart_id };
        const data = dataProxy.readQuery({ query, variables });
        const product = data.ListCartProducts.result.find(item => item.id === UpdateProduct.result.id);

        if (product) {
          Object.keys(UpdateProduct.result).forEach(key => {
            if (UpdateProduct.result[key]) {
              product[key] = UpdateProduct.result[key];
            }
          });

          data.ListCartProducts.result = data.ListCartProducts.result.filter(item => item.id !== UpdateProduct.result.id);
          data.ListCartProducts.result.push(product);
          dataProxy.writeQuery({ query, variables, data });
        }
        else {
          data.ListCartProducts.result.push(UpdateProduct.result);
          dataProxy.writeQuery({ query, variables, data });
        }
      },
      refetchQueries: mutationResults => [{
        query: GET_PRODUCTS_LIST,
        variables: { cart_id: mutationResults.data.UpdateProduct.result.cart_id }
      }]

    }
  }),
  graphql(DELETE_CART_PRODUCT, {
    props: props => ({
      DeleteProduct: data => props.mutate({
        variables: {
          id: data.id,
          cart_id: data.cart_id
        },
        optimisticResponse: () => ({
          DeleteProduct: {
            result: {
              id: data.id,
              cart_id: data.cart_id,
              __typename: 'Product'
            },
            __typename: 'CheckListCartProducts'
          }
        })
      })
    }),
    options: {
      fetchPolicy: 'cache-and-network',
      update: (dataProxy, { data: { DeleteProduct } }) => {
        const query = GET_PRODUCTS_LIST;
        const variables = { cart_id: DeleteProduct.result.cart_id };
        const data = dataProxy.readQuery({ query, variables });

        data.ListCartProducts.result = data.ListCartProducts.result.filter(product => product.id !== DeleteProduct.result.id);
        dataProxy.writeQuery({ query, variables, data });
      },
      refetchQueries: mutationResults => [{
        query: GET_PRODUCTS_LIST,
        variables: { cart_id: mutationResults.data.DeleteProduct.result.cart_id }
      }]
    }
  })
)(ShoppingCartComponent);
