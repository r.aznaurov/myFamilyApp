import React, { PureComponent } from 'react';
import { SwipeRow, View, Text, Icon, Button } from 'native-base';

export class ShoppingCartItem extends PureComponent {
  getItemColor() {
    switch(this.props.type) {
      case 'food':
        return '#e7fcb5';
      case 'household':
        return '#bfd7fc';
      case 'alcohol':
        return '#fcbfec';
      case 'sweets':
        return '#e994ef';
      default: return '#e7fcb5';
    }
  }

  render () {
    const {
      id,
      name,
      quantity,
      units,
      type,
      created_by,
      created_at,
      updated_by,
      updated_at,
      bought_by,
      bought_at
    } = this.props;

    return (
      <SwipeRow
        ref={ c => { this.component = c; }}
        leftOpenValue={75}
        rightOpenValue={-75}
        right={ bought_at
          ? <Button
            light
            onPress={() => {this.props.onBuy(
              id,
              name,
              quantity,
              units,
              type,
              created_by,
              created_at,
              updated_by,
              updated_at,
              bought_by,
              bought_at
            );
            this.component._root.closeRow();}}
          >
            <Icon active name="close" />
          </Button>
          : <Button
            success
            onPress={() => {this.props.onBuy(
              id,
              name,
              quantity,
              units,
              type,
              created_by,
              created_at,
              updated_by,
              updated_at,
              bought_by,
              bought_at
            );
            this.component._root.closeRow();} }
          >
            <Icon active name="checkmark" />
          </Button>
        }
        body={
          <View style={{flexDirection: 'row', justifyContent: 'flex-start', flex: 1}}>
            <View style={{width: 5, height: 20, backgroundColor: this.getItemColor()}}/>
            <Text style={
              bought_at ? {textDecorationLine: 'line-through', textDecorationStyle: 'solid', marginLeft: 30} : {marginLeft: 30}
            }>{ name }</Text>
          </View>
        }
        left={
          <Button danger onPress={() => this.props.onDelete(id)}>
            <Icon active name="trash" />
          </Button>

        }
        style={bought_at
          ? {borderBottomWidth: .33, borderBottomColor: '#c8c8c8', backgroundColor: '#efefef'}
          : {borderBottomWidth: .33, borderBottomColor: '#c8c8c8', backgroundColor: '#fff'}
        }
      />
    );
  }
}

