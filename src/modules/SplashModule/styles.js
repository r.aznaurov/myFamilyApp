import { StyleSheet } from 'react-native';
import { COLORS } from '../../constants';

export default StyleSheet.create({
  splashContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoHolder: {
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  logoImage: {
    width: 200,
    height: 200
  },
  loaderWrapper: {
    width: 300,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center'
  },
  animationStyle: {
    width: 300,
    height: 200,
    alignSelf: 'center'
  },
  loadingText: {
    color: COLORS.PRIMARY
  }
});
