import React, { Component } from 'react';
import { Container } from 'native-base';
import { View, Image, Text } from 'react-native';
import { Auth } from 'aws-amplify';
import { graphql, withApollo, compose } from 'react-apollo';
import { withNavigation } from 'react-navigation';
import Animation from 'lottie-react-native';
import get from 'lodash.get';
import { GET_ME } from '../../Queries/getMEQuery';
import anim from '../../static/loader/primaryloader.json';
import { LOGO } from '../../constants';
import styles from './styles';

class SplashModuleComponent extends Component {

  componentDidMount() {
    this.animation.play();
  }

  checkFieldsInMe(data) {
    const ME = get(data, 'data.Me') || {};
    const { first_name, last_name, role, family_id } = ME;

    if (!first_name || !last_name || !role) {
      return this.props.navigation.navigate('SetProfile');
    } else if (!family_id) {
      return this.props.navigation.navigate('JoinCreate');
    } else {
      return this.props.navigation.navigate('Dashboard');
    }
  }

  render() {
    Auth.currentAuthenticatedUser()
      .then(res => {
        this.props.client.query({
          query: GET_ME
          // options: {
          //   fetchPolicy: 'cache-and-network'
          // }
        })
          .then(res => {
            // console.warn( 'CLIENT', res);
            this.checkFieldsInMe(res);
          })
          .catch(err => {
            console.warn( 'CLIENTERROR', err);
          });
      })
      .catch(err => {
        this.props.navigation.navigate('SignIn');
      });

    return (
      <Container style={ styles.splashContainer }>
        <View style={ styles.containerWrapper }>
          <View style={ styles.logoHolder }>
            <Image style={ styles.logoImage } source={ LOGO }/>
          </View>
          <View style={ styles.loaderWrapper }>
            <Animation
              ref={ animation => {
                this.animation = animation;
              } }
              style={ styles.loaderWrapper }
              loop={ true }
              source={ anim }
            />
          </View>
          {/*{*/}
          {/*this.props.data.loading*/}
          {/*? <Text style={styles.loadingText}>Loading...</Text>*/}
          {/*: null*/}
          {/*}*/}
        </View>
      </Container>
    );
  }
}

export const SplashModule = compose(
  // graphql(GET_ME, {
  //   options: {
  //     fetchPolicy: 'cache-and-network'
  //   }
  // }),
  withApollo,
  withNavigation
)(SplashModuleComponent);
