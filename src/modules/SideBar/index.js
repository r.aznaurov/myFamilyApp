import React, { Component } from 'react';
import { Image } from 'react-native';
import { Auth } from 'aws-amplify';
import { withNavigation } from 'react-navigation';
import { LOGO } from '../../constants';
import { View } from 'react-native';
import {
  Text,
  Container,
  List,
  ListItem,
  Content
} from 'native-base';
import styles from './styles';

const routes = ['Dashboard', 'Settings', 'Profile', 'InviteMember'];

class SideBarComponent extends React.Component {
  signOut() {
    Auth.signOut()
      .then(data => {
        console.log(data);
        this.props.navigation.navigate('SignIn');
      })
      .catch(err => {
        console.log('Error:', err);
      });
  }

  render() {
    return (
      <Container>
        <Content>
          <View
            style={ styles.sideBarWrapper }
          />
          <Image
            square
            style={ styles.sideBarLogoImg }
            source={ LOGO }
          />
          <List
            dataArray={ routes }
            contentContainerStyle={ styles.sideBarListContainer }
            renderRow={ data => (
              <ListItem
                button
                onPress={ () => this.props.navigation.navigate(data) }
              >
                <Text>{ data }</Text>
              </ListItem>
            ) }
          />
          <ListItem
            button
            onPress={ () => this.signOut() }
          >
            <Text>
              Sign out
            </Text>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

export const SideBar = withNavigation(SideBarComponent);
