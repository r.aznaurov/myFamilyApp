import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  sideBarWrapper: {
    height: 120,
    width: '100%',
    alignSelf: 'stretch',
    position: 'absolute'
  },
  sideBarLogoImg: {
    height: 80,
    width: 80,
    position: 'absolute',
    alignSelf: 'center',
    top: 20
  },
  sideBarListContainer: {
    marginTop: 120
  },

});
