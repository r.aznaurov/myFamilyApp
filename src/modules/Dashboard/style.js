import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  dashboardContainer: {
    flex: 1
  },
  sectionsListContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  categoryButton: {
    marginBottom: 15,
    justifyContent: 'flex-start'
  },
  categoryText: {
    color: '#fff'
  },
  categoryIcon: {
    marginRight: 15
  }

});
