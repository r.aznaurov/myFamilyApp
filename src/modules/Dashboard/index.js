import React, { PureComponent } from 'react';
import { withApollo, graphql, compose } from 'react-apollo';
import { withNavigation } from 'react-navigation';
import { View, Text} from 'react-native';
import { Button, Icon } from 'native-base';
import { Container } from '../../components/Container';
import { GET_ME } from '../../Queries/getMEQuery';
import { GET_FAMILY_MEMBERS } from '../../Queries/getFamilyMembers';
import styles from './style';

class DashboardComponent extends PureComponent {

  render () {
    // console.warn(this.props.FamilyMembers);
    return (
      <Container>
        <View style={styles.sectionsListContainer}>
          <Button
            rounded
            iconLeft
            block
            info
            style={styles.categoryButton}
            onPress={() => this.props.navigation.navigate('ShoppingCartList')}
          >
            <Icon name='basket' style={styles.categoryIcon}/>
            <Text style={styles.categoryText}>SHOPPING CART</Text>
          </Button>
          <Button
            rounded
            iconLeft
            block
            info
            style={styles.categoryButton}
          >
            <Icon name='calendar' style={styles.categoryIcon}/>
            <Text style={styles.categoryText}>
              MY FAMILY CALENDAR
            </Text>
          </Button>
          <Button
            rounded
            iconLeft
            block
            info
            style={styles.categoryButton}
          >
            <Icon name='star' style={styles.categoryIcon}/>
            <Text style={styles.categoryText}>
              MY FAMILY WISH LIST
            </Text>
          </Button>
        </View>
      </Container>
    );
  }
}

export const Dashboard = compose(
  graphql(GET_FAMILY_MEMBERS, {name: 'FamilyMembers'}),
  graphql(GET_ME, {name: 'ME'}),
  withApollo,
  withNavigation
)(DashboardComponent);
