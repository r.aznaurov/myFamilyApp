import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import styles from './style';

export class Container extends Component {
  render() {
    return(
      <View style={styles.container}>
        <ScrollView>
          {this.props.children}
        </ScrollView>
      </View>
    );
  }
}
