import React, { Component } from 'react';
import { Button, Text, Spinner } from 'native-base';
import styles from '../../modules/Auth/styles';

export class SubmitButton extends Component {
  render() {
    const {onPress, children, submitProcessing, disabled, customStyle} = this.props;
    return (
      <Button
        block
        info
        rounded
        style={ customStyle ? [styles.submitBtn, customStyle] : styles.submitBtn }
        onPress={ onPress }
        disabled={disabled}
      >
        {
          submitProcessing
            ? <Spinner color='#fff'/>
            : <Text>{children}</Text>
        }
      </Button>
    );
  }
}
