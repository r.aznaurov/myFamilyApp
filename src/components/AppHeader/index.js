import React, { Component } from 'react';
import {
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title
} from 'native-base';
import { HEADER_TYPES } from '../../constants';

export class AppHeader extends Component {
  render() {
    const { options } = this.props.scene.descriptor;

    return (
      <Header>
        {
          this.props.type === HEADER_TYPES.PRIMARY
            ? <Left>
              <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name="menu" />
              </Button>
            </Left>
            : <Left>
              <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
        }
        <Body>
          <Title>{ options.title }</Title>
        </Body>
      </Header>
    );
  }
}
