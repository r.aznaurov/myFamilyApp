import React, { Component } from 'react';
import { Text } from 'native-base';
import { View } from 'react-native';

export class CountdownTimer extends Component {
  constructor() {
    super();
    this.state = {
      time: {},
      seconds: null
    };

    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  secondsToTime(secs){
    const divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    if (minutes.toString().length === 1) {
      minutes = `0${minutes}`;
    }

    const divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    if (seconds.toString().length === 1) {
      seconds = `0${seconds}`;
    }

    const obj = {
      'm': minutes,
      's': seconds
    };
    return obj;
  }

  componentDidMount() {
    this.setState({seconds: this.props.seconds},
      () => {
        const timeLeftVar = this.secondsToTime(this.state.seconds);
        this.setState({ time: timeLeftVar, seconds: this.props.seconds });
        this.startTimer();
      }
    );
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }


  startTimer() {
    if (this.timer === 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    const seconds = this.state.seconds - 1;

    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds
    });

    if (seconds === 0) {
      this.props.onTimerEnd();
      clearInterval(this.timer);
    }
  }

  render() {
    return(
      <View styles={this.props.timerContainerStyle}>
        <Text style={this.props.timerTextStyle}>{this.state.time.m}:{this.state.time.s}</Text>
      </View>
    );
  }
}
