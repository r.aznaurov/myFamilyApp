import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  formItem: {
    marginBottom: 20
  },
  withError: {
    marginBottom: 0
  },
  errorText: {
    marginBottom: 20,
    paddingLeft: 20,
    fontSize: 10,
    color: 'red'
  }
});
