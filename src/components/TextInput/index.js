import React, { Component } from 'react';
import { Item, Input, Text, Icon } from 'native-base';
import { View } from 'react-native';

import styles from './styles';

export class TextInput extends Component {
  render() {
    const {
      error,
      onBlur,
      onChangeText,
      placeholder,
      secureTextEntry,
      textStyle,
      keyboardType
    } = this.props;

    return (
      <View>
        <Item
          rounded
          style={ error ? [ styles.formItem, styles.withError ] : styles.formItem }
          error={ error !== null }
        >
          <Input
            keyboardType={keyboardType}
            style={ textStyle }
            value={ this.props.value }
            onChangeText={onChangeText}
            placeholder={ placeholder }
            onBlur={onBlur}
            secureTextEntry={ secureTextEntry }
          />
          {
            error
              ? <Icon name='close-circle' />
              : null
          }
        </Item>
        {
          error
            ? <Text style={styles.errorText}>
              { error }
            </Text>
            : null
        }
      </View>
    );
  }
}

