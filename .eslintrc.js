module.exports = {

  'globals': {
    'BEATIT_STORAGE': true
  },
  'parser': 'babel-eslint',
  'env': {
    'react-native/react-native': true,
    'es6': true
  },
  'extends': ['eslint:recommended', 'plugin:react-native/all'],
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 2018,
    'sourceType': 'module'
  },
  'plugins': [
    'react-native',
    'react'
  ],
  'rules': {
    'indent': [
      'error',
      2,
      {
        'SwitchCase': 1,
        'MemberExpression': 'off'
      }
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'react-native/no-unused-styles': 1,
    'react-native/split-platform-components': 1,
    'react-native/no-inline-styles': 1,
    'react-native/no-color-literals': 1,
    'react/jsx-uses-vars': 1,
    'react/display-name': 1,
    'no-debugger': 1,
    'no-dupe-args': 1,
    'no-dupe-keys': 1,
    'no-duplicate-case': 1,
    'no-empty': 1,
    'no-func-assign': 1,
    'no-unreachable': 1,
    'default-case': 1,
    'dot-location': ['error', 'property'],
    'no-empty-function': 1,
    'eol-last': 1,
    'comma-dangle': 1,
    'comma-spacing': 1,
    'no-multiple-empty-lines': ['error', {
      'max': 2
    }],
    'no-unused-vars': 0,
    'no-tabs': 1,
    'no-trailing-spaces': 1,
    'arrow-body-style': 1,
    'arrow-parens': ['error', 'as-needed'],
    'arrow-spacing': 1,
    'no-duplicate-imports': 1,
    'no-useless-rename': 1,
    'no-var': 1,
    'prefer-arrow-callback': 1,
    'prefer-const': 1,
    'switch-colon-spacing': ['error', { 'after': true, 'before': false }]
  }
};

